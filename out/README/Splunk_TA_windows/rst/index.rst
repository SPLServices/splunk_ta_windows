
Welcome to plunk Add-on for Microsoft Windows README!
*****************************************************

Documentation is provided in README.epub as well as online


LICENSE
=======

SPLUNK

See the License File(s) in the root of the package.


Support/Contributing
====================

This application is community supported. See the project repository
for more information include source and issue tracker. `Repository
<https://bitbucket.org/SPLServices/splunk_ta_windows/>`_
